

# Warning: This is an automatically generated file, do not edit!

srcdir=.
top_srcdir=.

include $(top_srcdir)/Makefile.include
include $(top_srcdir)/config.make

ifeq ($(CONFIG),DEBUG)
ASSEMBLY_COMPILER_COMMAND = mcs
ASSEMBLY_COMPILER_FLAGS =  -noconfig -codepage:utf8 -warn:4 -optimize+ -debug -define:DEBUG
ASSEMBLY = bin/Debug/GameServer.exe
ASSEMBLY_MDB = $(ASSEMBLY).mdb
COMPILE_TARGET = exe
PROJECT_REFERENCES = 
BUILD_DIR = bin/Debug

DB4OBJECTS_DB4O_DLL_SOURCE=dll/Db4objects.Db4o.dll

endif

ifeq ($(CONFIG),RELEASE)
ASSEMBLY_COMPILER_COMMAND = mcs
ASSEMBLY_COMPILER_FLAGS =  -noconfig -codepage:utf8 -warn:4 -optimize+
ASSEMBLY = bin/Release/GameServer.exe
ASSEMBLY_MDB = 
COMPILE_TARGET = exe
PROJECT_REFERENCES = 
BUILD_DIR = bin/Release

DB4OBJECTS_DB4O_DLL_SOURCE=dll/Db4objects.Db4o.dll

endif


PROGRAMFILES = \
	$(DB4OBJECTS_DB4O_DLL)  

BINARIES = \
	$(GAMESERVER)  



GAMESERVER = $(BUILD_DIR)/gameserver
DB4OBJECTS_DB4O_DLL = $(BUILD_DIR)/Db4objects.Db4o.dll


FILES = \
	Main.cs \
	AssemblyInfo.cs \
	ConsoleUI.cs \
	GameComponents/Character.cs \
	GameComponents/Item.cs \
	GameComponents/MOB.cs \
	GameComponents/Magic.cs \
	GameComponents/Skill.cs 

DATA_FILES = 

RESOURCES = 

EXTRAS = \
	GameServer.userprefs \
	GameServer.usertasks \
	Packages.mdse \
	GameServer.make \
	autogen.sh \
	configure.ac \
	gameserver.in \
	config.log \
	config.make \
	configure \
	gameserver \
	rules.make \
	bin/Debug/GameServer.exe.mdb \
	bin/Debug/gameserver \
	data/gameserver.in \
	gameserver.in 

REFERENCES =  \
	System

DLL_REFERENCES =  \
	dll/Db4objects.Db4o.dll

CLEANFILES += $(PROGRAMFILES) $(BINARIES) 

#Targets
all-local: $(ASSEMBLY) $(PROGRAMFILES) $(BINARIES)  $(top_srcdir)/config.make

$(GAMESERVER): gameserver
	mkdir -p $(BUILD_DIR)
	cp '$<' '$@'
	chmod u+x '$@'

$(DB4OBJECTS_DB4O_DLL): $(DB4OBJECTS_DB4O_DLL_SOURCE)
	mkdir -p $(BUILD_DIR)
	cp '$<' '$@'



gameserver: gameserver.in $(top_srcdir)/config.make
	sed -e "s,@prefix@,$(prefix)," -e "s,@PACKAGE@,$(PACKAGE)," < gameserver.in > gameserver


$(build_xamlg_list): %.xaml.g.cs: %.xaml
	xamlg '$<'

$(build_resx_resources) : %.resources: %.resx
	resgen '$<' '$@'



$(ASSEMBLY) $(ASSEMBLY_MDB): $(build_sources) $(build_resources) $(build_datafiles) $(DLL_REFERENCES) $(PROJECT_REFERENCES) $(build_xamlg_list)
	make pre-all-local-hook prefix=$(prefix)
	mkdir -p $(dir $(ASSEMBLY))
	make $(CONFIG)_BeforeBuild
	$(ASSEMBLY_COMPILER_COMMAND) $(ASSEMBLY_COMPILER_FLAGS) -out:$(ASSEMBLY) -target:$(COMPILE_TARGET) $(build_sources_embed) $(build_resources_embed) $(build_references_ref)
	make $(CONFIG)_AfterBuild
	make post-all-local-hook prefix=$(prefix)


install-local: $(ASSEMBLY) $(ASSEMBLY_MDB) $(GAMESERVER) $(DB4OBJECTS_DB4O_DLL)
	make pre-install-local-hook prefix=$(prefix)
	mkdir -p $(prefix)/lib/$(PACKAGE)
	cp $(ASSEMBLY) $(ASSEMBLY_MDB) $(prefix)/lib/$(PACKAGE)
	mkdir -p $(prefix)/bin
	test -z '$(GAMESERVER)' || cp $(GAMESERVER) $(prefix)/bin
	test -z '$(DB4OBJECTS_DB4O_DLL)' || cp $(DB4OBJECTS_DB4O_DLL) $(prefix)/lib/$(PACKAGE)
	make post-install-local-hook prefix=$(prefix)

uninstall-local: $(ASSEMBLY) $(ASSEMBLY_MDB) $(GAMESERVER) $(DB4OBJECTS_DB4O_DLL)
	make pre-uninstall-local-hook prefix=$(prefix)
	rm -f $(prefix)/lib/$(PACKAGE)/$(notdir $(ASSEMBLY))
	test -z '$(ASSEMBLY_MDB)' || rm -f $(prefix)/lib/$(PACKAGE)/$(notdir $(ASSEMBLY_MDB))
	test -z '$(GAMESERVER)' || rm -f $(prefix)/bin/$(notdir $(GAMESERVER))
	test -z '$(DB4OBJECTS_DB4O_DLL)' || rm -f $(prefix)/lib/$(PACKAGE)/$(notdir $(DB4OBJECTS_DB4O_DLL))
	make post-uninstall-local-hook prefix=$(prefix)
