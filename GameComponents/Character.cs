﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wakedown.GameServer.GameComponents
{
    class Character
    {
        private uint id;
        private string name;
        private IList<Item> items;
        private IList<Skill> skills;
        private IList<Magic> magics;
        private IList<Magic> spellbook;
        private DateTime lastLogin;
        private DateTime created;
    }
}
