﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wakedown.GameServer
{
    class ConsoleUI
    {
        public ConsoleUI()
        {
            Initializate();
        }

        static void Initializate()
        {
            Console.WriteLine("Welcome to Wakedown's GameServer console.");
            ConsoleKeyInfo key;
            
            while (ReadSomeCommand(out key))
            {
                
            }
        }

        static bool ReadSomeCommand(out ConsoleKeyInfo key)
        {
            Console.Write(">");
            key = Console.ReadKey();
            Console.WriteLine();
            if (key.Key == ConsoleKey.Escape)
                return false;
            return true;
        }
    }
}
